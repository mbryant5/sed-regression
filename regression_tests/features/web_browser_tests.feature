Feature: Web Browser Tests

  Scenario Outline:
    Given I want to install <application>
    When I go to <link> using <browser>
    And I navigate to <page>
    Then <file> should download
    Examples:
    | application          | link                                    | browser  | page            | file          |
    | SED Bootstrapper     | https://qa.sageexchange.com/install/    | firefox  |                 |               |
    | SED Distributor      | https://qa.sageexchange.com/install/    | firefox  |                 |               |